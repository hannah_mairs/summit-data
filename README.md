This repository contains data collected from runs of PETSc program hannah-vec-ops.c on Summit at the OLCF. Also includes Matplotlib plotting scripts.

The data files vec_ops.* have the naming conventions:

n: number of resource sets

g: number of GPUs per resource set

c: number of CPUs per resource set

p: total number of MPI ranks

a: number of MPI ranks per resource set

vector size

Summit run number